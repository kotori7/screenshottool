var express = require("express");
var multer = require("multer");
var fs = require("fs");
var upload = multer({dest: 'temp/'});
var mime = require("mime-types");

var app = express();
var key = "key";
var port = process.env.PORT;

app.post('/upload', upload.single('file'), function(req, res, next){
  if(req.get('Token') != key){
    res.status(403).send("Invalid token.");
    fs.unlinkSync(req.file.path);
    return;
  }
  var nm = "data/" + req.file.originalname;
  fs.renameSync("temp/" + req.file.filename, nm)
  res.send(req.file.originalname);
});
app.get('/i/:name', function(req, res, next){
  var aaa = req.params;
  if(aaa.name == ""){
    res.status(400).send("No file name specified.");
    return;
  }
  if(!fs.existsSync("data/" + aaa.name)){
    res.status(404).send("File not found.");
    return;
  }
  if(aaa.name.includes("..")){
    res.status(420).send("fuck off");
    return;
  }
  var mt = mime.lookup("data/" + aaa.name) || "application/octet-stream";
  fs.readFile("data/" + aaa.name, (err, data) => {
    if(err){
      res.status(500).send(err);
      return;
    }
    res.set("Content-Type", mt);
    res.send(data);
  });
});
app.get('/', function(req, res, next){
  fs.readFile("ok.jpg", (err, data) => {
    if(err) next();
    res.set("Content-Type", "image/jpeg");
    res.send(data);
  });
});

app.listen(port, () => console.log("listening on port %d...", port))
